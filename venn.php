<?php

/**
 * Create a venn diagram
 *
 * Usage: /venn item 1, item 2
 */

if (isset($_GET['item1']) && isset($_GET['item2'])) {
    $circleSize = 500;
    $fontSize = 60;
    $fontFile = 'Roboto-Regular';

    $wordSpacing = 30;
    $gutterSpacing = 30;

    // This is just to prevent calculations being done over and over
    $quarterSize = $circleSize / 4;
    $halfSize = $circleSize / 2;
    $doubleSize = $circleSize * 2;

    // Render image
    $image = imagecreatetruecolor($doubleSize - $halfSize + ($gutterSpacing * 2), $circleSize + ($gutterSpacing * 2));

    // Set transparency
    imagealphablending($image, true);
    imagesavealpha($image, true);
    imagefill($image, 0, 0, 0x7f000000);

    // Draw circles
    $leftCircle = imagecolorallocate($image, 0, 0, 0x300000ff);
    imagefilledellipse($image, $halfSize + $gutterSpacing, $halfSize + $gutterSpacing, $circleSize, $circleSize, $leftCircle);

    $rightCircle = imagecolorallocate($image, 0, 0, 0x3000cc00);
    imagefilledellipse($image, $circleSize + $gutterSpacing, $halfSize + $gutterSpacing, $circleSize, $circleSize, $rightCircle);

    // Add text
    putenv('GDFONTPATH=' . realpath('.'));

    // Determine box size required, we have a box approx 50% the size of the circle, try to set at midpoint
    $items = array(ucwords(htmlspecialchars(urldecode($_GET['item1']))), 'Sweet Spot', ucwords(htmlspecialchars(urldecode($_GET['item2']))));

    $wordPositions = array();
    $textColour = imagecolorallocate($image, 0, 0, 0);

    foreach ($items as $item => $words) {
        $words = explode(' ', $words);
        $sections[$item] = array();

        $maxWidth = $halfSize - $gutterSpacing;
        $maxHeight = $quarterSize;
        $sectionFontSize = $fontSize;
        $overallHeight = 0;

        while (!$overallHeight || $overallHeight > $maxHeight) {
            // Reset
            $sections[$item]['fontSize'] = $sectionFontSize;
            $sections[$item]['lines'] = array();
            $lineCount = 0;
            $lastWord = '';

            foreach ($words as $index => $word) {
                $testWord = trim($lastWord . ' ' . $word);
                $testData = imagettfbbox($sectionFontSize, 0, $fontFile, $testWord);
                $width = $testData[2] - $testData[3];

                // If width of these words is too big split
                if ($width > $maxWidth) {
                    $lastWord = $word;

                    $wordData = imagettfbbox($sectionFontSize, 0, $fontFile, $word);
                    $width = $wordData[2] - $wordData[3];

                    if ($width > $maxWidth) {
                        // Reduce font size and start again
                        $sectionFontSize = $sectionFontSize * 0.8;

                        continue 2;
                    }

                    ++$lineCount;
                } else {
                    $lastWord = $testWord;
                }

                $sections[$item]['lines'][$lineCount] = array(
                    'words' => $lastWord,
                    'width' => $width,
                );
            }

            // Check height
            $overallHeight = ($sectionFontSize * $lineCount) + (($sectionFontSize / 2) * ($lineCount - 1));

            // Reduce font size
            $sectionFontSize = $sectionFontSize * 0.8;
        }
    }

    // Position words on image
    foreach ($sections as $index => $sectionData) {
        // Determine y starting position
        $y = $gutterSpacing + $halfSize + ($sectionData['fontSize'] / 2) - (($sectionData['fontSize'] * (count($sectionData['lines']) - 1) + (($sectionData['fontSize'] / 2) * (count($sectionData['lines']) - 1))) / 2);
        $lineCount = 0;

        foreach ($sectionData['lines'] as $line => $data) {
            if ($lineCount > 0) {
                $y = $y + $sectionData['fontSize'] + ($sectionData['fontSize'] / 2);
            }

            // We always need to account for the gutter for the image
            $x = $gutterSpacing;

            // Offset the words further to the mid point of where the words sit
            switch ($index) {
                case 0:
                    // Left item
                    $x += $quarterSize;
                    break;

                case 1:
                    // Middle item
                    $x += ($halfSize + $quarterSize);
                    break;

                case 2:
                    // Right item
                    $x += $circleSize + $quarterSize;
                    break;
            }

            imagettftext($image, $sectionData['fontSize'], 0, $x - ($data['width'] / 2), $y, $textColour, $fontFile, $data['words']);
            ++$lineCount;
        }
    }

    // Output as PNG
    header("Content-type: image/png");
    imagepng($image);
    exit;
}

require_once 'slack.php';

$slack = new Slack('********************TOKEN********************');

if (!$slack->getText()) {
    return $slack->sendMessage('You need to specify some text to use for the venn diagram');
}

if (strpos($slack->getText(), ',') === false || substr_count($slack->getText(), ',') > 1) {
    return $slack->sendMessage('You need to specify exactly 2 items for the venn diagram separated by a comma');
}

$items = explode(',', $slack->getText(), 2);

$link = str_replace('.coredna.com', '.corednacdn.com', $slack->getUrl(true)) . '?item1=' . rawurlencode(trim($items[0])) . '&item2=' . rawurlencode(trim($items[1]));

// Prefetch
file_get_contents($link);

// Return venn URL
return $slack->sendReply('|<|' . $link . '|*' . htmlspecialchars($slack->getText()) . '*|>|');

<?php

/**
 * Send the not my problem gif
 *
 * Usage: /notmyproblem
 */

require_once 'slack.php';

$slack = new Slack('********************TOKEN********************');

// Simply reply with the gif
return $slack->sendReply("|<|https://media.giphy.com/media/ToMjGpIyKqTPR8C8N1u/giphy.gif|*Not my problem*|>|");

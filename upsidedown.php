<?php

/**
 * Write some text upside down
 *
 * Usage: /upsidedown text
 */

require_once 'slack.php';

$slack = new Slack('********************TOKEN********************');

// Get text and process each letter
$message = '';
$text = strrev($slack->getText());

for ($i = 0; $i < strlen($text); ++$i) {
    $letter = substr($text, $i, 1);

    $orig = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '`', '!', '&', '(', ')', "'", '"', ',', '.', '?', '<', '>');
    $repl = array('ɐ', 'q', 'ɔ', 'p', 'ǝ', 'ɟ', 'ƃ', 'ɥ', 'ı', 'ɾ', 'ʞ', 'ן', 'ɯ', 'u', 'o', 'd', 'b', 'ɹ', 's', 'ʇ', 'n', 'ʌ', 'ʍ', 'x', 'ʎ', 'z', '∀', 'q', 'Ɔ', 'p', 'Ǝ', 'Ⅎ', 'פ', 'H', 'I', 'ſ', 'ʞ', '˥', 'W', 'N', 'O', 'Ԁ', 'Q', 'ɹ', 'S', '┴', '∩', 'Λ', 'M', 'X', '⅄', 'Z', 'Ɩ', 'ᄅ', 'Ɛ', 'ㄣ', 'ϛ', '9', 'ㄥ', '8', '6', '0', ',', '¡', '⅋', ')', '(', ',', ',,', "'", '˙', '¿', '>', '<');

    $key = array_search($letter, $orig);

    $message .= ($key === false) ? $letter : $repl[$key];
}

// Create and send reply in reverse
return $slack->sendReply($message);

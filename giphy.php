<?php

/**
 * Get a gif from giphy based on a string
 *
 * Usage: /giphy text
 */

require_once 'slack.php';

$slack = new Slack('********************TOKEN********************');

if (!$slack->getText()) {
    return $slack->sendMessage('You need to specify a search term, e.g. */giphy much wow*, if you need ideas you could try */giphy trending* to get the top trending gif right now');
}

// Get the matching gif from giphy
switch (mb_strtolower($slack->getText())) {
    case 'trending':
        $gif = getGif(mb_strtolower($slack->getText()));
        break;

    default:
        $gif = getGif('search', $slack->getText());
        break;
}

if (!$gif instanceof StdClass || !is_array($gif->data) || !isset($gif->data[0]->images)) {
    return $slack->sendMessage('No gif found for "*' . $slack->getText() . '*", if you need ideas you could try *trending* to get the top trending gif right now');
}

// Check filesize, there is a 2MB limit by default, slack calculates this as 1000 * 1000 * 2
$gifUrl = ($gif->data[0]->images->original->size > (2000000)) ? $gif->data[0]->images->downsized->url : $gif->data[0]->images->original->url;

// Send reply to slack
return $slack->sendReply('|<|' . $gifUrl . '|*' . htmlspecialchars(urldecode($slack->getText())) . '*|>|');

/**
 * Get a gif from the giphy api
 *
 * @param  string  $type
 * @param  string  $text
 * @return array
 */
function getGif($type, $text = null)
{
    switch ($type) {
        case 'search':
            $endpoint = 'search?api_key=dc6zaTOxFJmzC&limit=1&q=' . urlencode($text);
            break;

        case 'random':
        case 'trending':
            $endpoint = $type . '?api_key=dc6zaTOxFJmzC&limit=1';
            break;

        default:
            return false;
            break;
    }

    $request = curl_init('http://api.giphy.com/v1/gifs/' . $endpoint);
    curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($request);
    curl_close($request);

    return json_decode($response);
}

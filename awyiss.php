<?php

/**
 * Create an awyiss comic
 *
 * Usage: /awyiss text
 */

require_once 'slack.php';

$slack = new Slack('********************TOKEN********************');

if (!$slack->getText()) {
    return $slack->sendMessage('You need to specify some text to create a comic with');
}

if (strlen($slack->getText()) > 45) {
    return $slack->sendMessage('You need to specify some text that is less than 45 characters');
}

$data = getComic($slack->getText());

// Reply if we have a link
if ($data->link) {
    return $slack->sendReply('|<|' . $data->link . '|*Aw yiss*|>|');
}

// Send error
return $slack->sendMessage('Something went wrong, try again soon');

/**
 * Create an aw yiss comic
 *
 * @param  string  $text
 * @return array
 */
function getComic($text)
{
    $request = curl_init('http://awyisser.com/api/generator');
    curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($request, CURLOPT_POSTFIELDS, 'phrase=' . $text);
    curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($request);
    curl_close($request);

    return json_decode($response);
}

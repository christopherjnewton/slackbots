<?php

/**
 * Google something for someone
 *
 * Usage: /lmgtfy text
 */

require_once 'slack.php';

$slack = new Slack('********************TOKEN********************');

// Create and send link
return $slack->sendReply('|<|http://lmgtfy.com/?q=' . $slack->getText() . '|*Sure, let me Google that for you*|>|');
